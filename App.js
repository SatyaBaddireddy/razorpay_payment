/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Alert,
} from 'react-native';
import RazorpayCheckout from 'react-native-razorpay';
import Input from './Components/Input';

export class App extends Component {
  state = {
    name: '',
    email: '',
    amount: '',
    phone: '',
    remark: '',
    payment_done: false,
    payment_id: ''
  }
  makepayment() {
    console.log("coming");
    var options =
    {
      description: this.state.remark,
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: 'INR',
      key: 'rzp_test_EfXZG1beYxWb3B',
      amount: parseInt(this.state.amount) * 100, name: this.state.name,
      prefill: {
        email: this.state.email,
        contact: this.state.phone, name: this.state.name
      },
      theme: { color: '#53a20e' }
    }
    if (this.state.name != '' || this.state.phone != '' || this.state.email != '' || this.state.amount != '') {
      RazorpayCheckout.open(options).then((data) => {
        this.setState({ payment_done: true });
        this.setState({ payment_id: `${data.razorpay_payment_id}` })
        alert(`Success: ${data.razorpay_payment_id}`);
      }).catch((error) => {
        this.setState({ payment_done: false });
        alert(`Error: ${error.code} | ${error.description}`);
      });
    }
    else {
      Alert.alert('Payment Process', 'All fields are Required');
    }
  }

  setUsername = (val) => {
    this.setState({ name: val });
  }
  setUserEmail = (val) => {
    this.setState({ email: val });
  }
  setUserAmount = (val) => {
    this.setState({ amount: val });
  }
  setUserPhone = (val) => {
    this.setState({ phone: val });
  }
  setUserRemark = (val) => {
    this.setState({ remark: val });
  }
  Submit() {
    console.log(this.state.name, this.state.email, this.state.amount, this.state.phone, this.state.remark);
  }
  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <View style={styles.body}>
              <View style={{ margin: 20 }}>
                <Input label="Name" placeholder="Enter Your Name" type={'default'}
                 onChangeText={this.setUsername.bind(this)} />
                <Input label="Email" placeholder="Enter Your Email"
                 type={'default'}
                 onChangeText={this.setUserEmail.bind(this)} />
                <Input label="Amount" placeholder="Enter Your Amount to Send"
                type={'number-pad'}
                 onChangeText={this.setUserAmount.bind(this)} />
                <Input label="Phone" placeholder="Enter Your Phone Number"
                type={'number-pad'}
                onChangeText={this.setUserPhone.bind(this)} />
                <Input label="Remarks"
                type={'default'}
                 placeholder="Enter Remarks" onChangeText={this.setUserRemark.bind(this)} />
              </View>
              {/* <TouchableOpacity onPress={this.makepayment.bind(this)}>
              <Text>Submit</Text>
            </TouchableOpacity> */}
              <View style={{ justifyContent: 'space-evenly', margin: 20 }}>
                {this.state.payment_done == true ? <Text>Payment Success full : {this.state.payment_id}</Text> :

                  <View>
                    <TouchableOpacity onPress={() => this.makepayment()} style={{ backgroundColor: '#0078d4', height: 50, borderRadius: 10, justifyContent: 'center', alignItems: 'center', width: '100%' }}>

                      <Text style={{ color: '#ffffff', fontSize: 24 }}>
                        Pay with Razorpay
                        </Text>
                    </TouchableOpacity>
                  </View>
                }
              </View>

            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#eeeeee',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: '#fff',
    borderWidth:1,
    margin:10,
    borderRadius:10
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 10,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: '#222',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: '#222',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: '#eeeeee',
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
