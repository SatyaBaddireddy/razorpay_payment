import React,{Component} from 'react';
import { TextInput, View ,Text,Keyboard} from 'react-native';
export default class Input extends Component{
    constructor(props){
        super(props); 
    }
  
    render(){

        return(
            <View style={{marginBottom:10}}>
                <View>
                <Text style={{marginLeft:4}}>{this.props.label}</Text>
                </View>
                <TextInput style={{borderWidth:1,borderColor:'#ccc',marginTop:10}} 
                placeholder={this.props.placeholder} onChangeText={this.props.onChangeText}    keyboardType={this.props.type}       
                />
                
            </View>
        )
    }
}